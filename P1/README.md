# Advanced Mobile Application Development

## An Nguyen

### Assignment 1 Requirements:

> Create a music playlist app to play different types of songs

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme
5. Create and display launcher icon image

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s follow-up screen (with images and buttons);
4. Screenshots of running application’s play and pause user interfaces (with images and buttons);


#### Assignment Screenshots:

*Screenshot of splash screen*:

![Splash Screen](images/1.png)

*Screenshot of follow-up screen*:

![Follow-up Screen](images/2.png)

*Screenshot of play and pause interface of first song*:

![First song play](images/3.png)

*Screenshot of play and pause interface of second song*:

![Second song play](images/4.png)
