# Advanced Mobile Application Development

## An Nguyen

### Assignment 2 Requirements:

1. Drop-down menu for total number of guests (including yourself): 1 – 10
2. Drop-down menu for tip percentage (5% increments): 0 – 25
3. Must add background color(s) or theme
4. Extra Credit (10 pts): Create and display launcher icon image

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s unpopulated user interface;
3. Screenshot of running application’s populated user interface;

#### Assignment Screenshots:

*Screenshot of Unpopulated*:

![unpopulated](images/1.png)

*Screenshot of populated*:

![populated](images/2.png)
