# Advanced Mobile Application Development

## An Nguyen

### Assignment 1 Requirements:

#### README.md file should include the following items:

1. ScreenshotofrunningJDKjavaHello(#2above);
2. ScreenshotofrunningAndroidStudioMyFirstApp(#2above,examplebelow);
3. ScreenshotofrunningAndroidStudioContactsApp(examplebelow);
a. Mainscreenwith3buttons,andcontactinformationw/imageforatleast3contacts.
b. *Must*includebackgroundcoloranddesign—cannotuse“default”activitydesign.
c. *Be sure* to review Chs. 1 and 2 of the course textbook.
4. gitcommandsw/shortdescriptions;
5. Bitbucket repo links: a) this assignment and b) the completed tutorial repos above
(bitbucketstationlocations and myteamquotes).

#### Git commands w/short descriptions:

1. git init: creates new local repository with specified name
2. git status: lists all new or modified files to be committed
3. git add: snapshots the file in preparation in versioning
4. git commit: records file snapshots permanently in version history
5. git push: uploads all history from the repository bookmark
6. git pull: downloads bookmark history and incorporates changes
7. git rm: deletes the file from the working directory and stages the deletion

#### Assignment Screenshots:
>This large app provides business contact information in an address book. Create three screens for contacts for the app. Use the app to select a particular contact, and then display that person’s information.

*Screenshot of JDK running java.hello*:

![Running JDK Screenshot](images/1.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](images/2.png)

*Screenshot of Contacts App*:

![Contacts App] (images/3.png)

*Screenshot of when button clicked*:

![Contacts App2] (images/4.png)

#### Links to Repo:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/atn13/bitbucketstationlocations)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/atn13/myteamquotes)
