# Advanced Mobile Application Development

## An Nguyen

### Assignment 5 Requirements:

1. Include splash screen with app title and list of articles.
2. Must find and use your own RSS feed.
3. Must add background color(s) or theme
4. Create and display launcher icon image

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s splash screen (list of articles–activity_items.xml);
3. Screenshot of running application’s individual article (activity_item.xml);
4. Screenshots of running application’s default browser (article link);

#### Assignment Screenshots:

*Splash Image*:

![Splash Image](images/1.png)

*Items Activity*:

![Items Activity](images/2.png)

*Read More...*:

![Read More...](images/3.png)

