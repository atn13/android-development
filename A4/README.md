# Advanced Mobile Application Development

## An Nguyen

### Assignment 4 Requirements:

1. Include splash screen image, app title, intro text.
2. Include appropriate images.
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme
6. Create and display launcher icon image

#### README.md file should include the following items:

1. Coursetitle,yourname,assignmentrequirements,asperA1;
2. Screenshotofrunningapplication’ssplashscreen;
3. Screenshotofrunningapplication’sinvalidscreen(withappropriateimage);
4. Screenshotsofrunningapplication’svalidscreen(withappropriateimage);

#### Assignment Screenshots:

*Splash Image*:

![Splash Image](images/1.png)

*Opening Screen*:

![Opening Screen](images/2.png)

*Invalid Entry*:

![Invalid](images/3.png)

*Valid Entry*:

![Valid](images/4.png)
